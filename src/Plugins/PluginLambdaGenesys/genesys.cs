﻿/*  SourceRemoteControl
    Copyright (C) 2014  Sebastian Block

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using System;
using System.Data;
using SourcePluginInterface;
using System.IO;
using System.IO.Ports;
using System.Globalization;
using System.Windows.Forms;
using ClassDeviceInformation;

namespace PluginLambdaGenesys
{
    public class SourcePlugin : InterfaceSourcePlugin
    {
        private SerialPort serialPort;
        private float lastVoltage = 0;
        private ComboBox cbComport  = null;
        private ComboBox cbBaudrate = null;
        private ComboBox cbAddress  = null;
        private DeviceInformation devInfo = new DeviceInformation();
        private string configDir;

        /* Konstanten */
        const float F_OUTPUT_INVALID = (float) -10010.0;
        const int I_OUTPUT_INVALID = -10;

        public int Connect(string connectionInformation)
        {
            string s;
            if (cbComport == null) return 1;
            if (serialPort != null)
            {
                if (serialPort.IsOpen) serialPort.Close();
                if (serialPort.PortName != cbComport.SelectedText) serialPort.PortName = cbComport.SelectedItem.ToString();
            }
            else
            {
                //tbOut.AppendText("Open Port: " + connectionData + "\n\r");
                serialPort = new SerialPort(cbComport.SelectedItem.ToString());
                //serialPort.DataReceived += new SerialDataReceivedEventHandler(SerialDataRecvHandler);
            }

            serialPort.StopBits = StopBits.One;
            serialPort.Parity = Parity.None;
            serialPort.DataBits = 8;
            serialPort.BaudRate = Int32.Parse(cbBaudrate.SelectedItem.ToString());
            serialPort.Handshake = Handshake.None;
            serialPort.ReadTimeout = 500;

            serialPort.Open();

            s = TDKLambdaSendRecv("ADR " + Int32.Parse(cbAddress.Text));
            if (!s.Contains("OK"))
            {
                serialPort.Close();
                return 1;
            }


            s = TDKLambdaSendRecv("IDN?");

            if (s.Contains("LAMBDA,GEN"))
            {
                
                TDKLambdaSendCmd("RMT REM");
                
                TDKLambdaGetDeviceInformation();
                lastVoltage = GetVoltage(1);
                
                /** speichere Einstellungen */
                DataTable mySettings;

                // Create DataTable for Settings
                mySettings = new DataTable("SettingsPluginLambdaGenesys");
                mySettings.Columns.Add("ComPort", typeof(string));
                mySettings.Columns.Add("Baudrate", typeof(Int32));
                mySettings.Columns.Add("Address", typeof(Int32));
                
                mySettings.Rows.Add(new object[3] { 
                    cbComport.SelectedItem.ToString(),
                    Int32.Parse(cbBaudrate.SelectedItem.ToString()),
                    cbAddress.SelectedIndex,
                });

                mySettings.WriteXml(configDir + Path.DirectorySeparatorChar + "config_plugin_lambda_genesys.xml", XmlWriteMode.WriteSchema);

            }
            else
            {
                serialPort.Close();
                return 1;
            }
            return 0;
        }

        public int Disconnect()
        {
            if (serialPort == null) return 1;
            if (serialPort.IsOpen)
            {
                TDKLambdaSendCmd("RMT LOC");
                serialPort.Close();
            }
            return 0;
        }

        public int EnableOutput(int output, bool value)
        {
            if (output > devInfo.outputs || output < 1) return I_OUTPUT_INVALID;
            if (value)
                TDKLambdaSendCmd("OUT 1");
            else
                TDKLambdaSendCmd("OUT 0");
            return 0;
        }

        public int EnableOutputs(bool value)
        {
            if (value)
                TDKLambdaSendCmd("OUT 1");
            else
                TDKLambdaSendCmd("OUT 0");
            return 0;
        }

        public float GetCurrentLimit(int output)
        {
            if (output > devInfo.outputs || output < 1) return F_OUTPUT_INVALID;
            string s = TDKLambdaSendRecv("PC?");
            return float.Parse(s, CultureInfo.InvariantCulture);
        }

        public string GetPluginDate()
        {
            return "2017-03-15 14:00";
        }

        public string GetPluginName()
        {
            return "TDK-Lambda Genesys";
        }

        public int GetPluginVersion()
        {
            throw new NotImplementedException();
        }

        public float GetVoltage(int output)
        {
            if (output > devInfo.outputs || output < 1) return F_OUTPUT_INVALID;
            string s = TDKLambdaSendRecv("MV?");
            return float.Parse(s, CultureInfo.InvariantCulture);
        }

        public int SetCurrentLimit(int output, float current)
        {
            if (output > devInfo.outputs || output < 1) return I_OUTPUT_INVALID;
            string s = "PC " + current.ToString("F2", CultureInfo.InvariantCulture);
            TDKLambdaSendCmd(s);
            return 0;
        }

        public int SetVoltage(int output, float voltage)
        {
            if (output > devInfo.outputs || output < 1) return I_OUTPUT_INVALID;
            string s = "PV " + voltage.ToString("F2", CultureInfo.InvariantCulture);
            if (voltage > 0) lastVoltage = voltage;
            TDKLambdaSendCmd(s);
            return 0;
        }

        public bool isOutputEnabled(int output)
        {
            string s = TDKLambdaSendRecv("OUT?");
            if (s.Contains("ON"))
            {
                return true;
            }
            return false;
        }
        

        private string TDKLambdaSendRecv(string cmd)
        {
            int i = 0;
            int readBytes = 0;
            byte[] buf = new byte[1024];
            string s = "";
            if (serialPort.IsOpen == false) return s;

            cmd += "\r\n";
            System.Text.Encoding.ASCII.GetBytes(cmd, 0, cmd.Length, buf, 0);
            serialPort.Write(buf, 0, cmd.Length);

            i = 0;
            buf[0] = 0;
            try
            {
                do
                {
                    readBytes = serialPort.Read(buf, i, 1);
                    if (readBytes > 0)
                    {
                        i += readBytes;
                    }
                } while (buf[i - 1] != 0x0D);
                s = System.Text.Encoding.ASCII.GetString(buf, 0, i);
            }
            catch (TimeoutException)
            {
                s = "timeout waiting for device";
            }
            //System.Threading.Thread.Sleep(50);
            return s;
        }

        private void TDKLambdaSendCmd(string s)
        {
            //int counter = 10;
            if (serialPort.IsOpen)
            {
                string ok = TDKLambdaSendRecv(s);
                if (!ok.Contains("OK"))
                {
                    ok = "";
                }
            }
            //while (counter-- > 0) System.Threading.Thread.Sleep(10);
        }

        private void TDKLambdaGetDeviceInformation()
        {
            string s = TDKLambdaSendRecv("IDN?");
            string[] sList = s.Split(',');
            devInfo.producer = sList[0].Trim();
            devInfo.type = sList[1].Trim();

            s = TDKLambdaSendRecv("SN?");
            devInfo.serialNo = s.Trim();

            s = TDKLambdaSendRecv("REV?");
            devInfo.softwareRevision = s.Trim();

            sList = devInfo.type.Split('-');
            devInfo.currentMin = 0; /* all devices have a min current of 0 */
            devInfo.currentMax = int.Parse(sList[1].Trim());
            devInfo.currentLimitMin = devInfo.currentMin;
            devInfo.currentLimitMax = devInfo.currentMax;

            devInfo.voltageMin = 0;
            devInfo.voltageMax = int.Parse(sList[0].Split('N')[1].Trim());
            devInfo.voltageLimitMin = 0;
            devInfo.voltageLimitMax = devInfo.voltageMax;
            devInfo.voltageOVPMin = 0;
            devInfo.voltageOVPMax = devInfo.voltageMax;

            devInfo.outputs = 1;

            devInfo.singleTurnOnOff = true;
        }

        public int ConnectionSettingSetup(object panel, string confDir)
        {
            Panel pPanel = (Panel)panel;
            configDir = confDir;

            string comport;
            int baud, address;
            /** 
            * load settings  
            */
            try
            {
                // Create Settings Table
                DataTable mySettings = new DataTable("SettingsPluginLambdaGenesys");
                mySettings.ReadXml(confDir + Path.DirectorySeparatorChar + "config_plugin_lambda_genesys.xml");

                comport = mySettings.Rows[0]["ComPort"].ToString();
                baud = Convert.ToInt32(mySettings.Rows[0]["Baudrate"].ToString(), 10);
                address = Convert.ToInt32(mySettings.Rows[0]["Address"].ToString(), 10);
            }
            catch
            {
                comport = "COM1";
                baud = 9600;
                address = 0;
            }

            Label lblComport = new Label();
            lblComport.Text = "Comport:";
            lblComport.Top = 8;
            lblComport.Left = 5;
            lblComport.Width = 70;
            pPanel.Controls.Add(lblComport);

            Label lblBaudrate = new Label();
            lblBaudrate.Text = "Baudrate:";
            lblBaudrate.Top = 34;
            lblBaudrate.Left = 5;
            lblBaudrate.Width = 70;
            pPanel.Controls.Add(lblBaudrate);

            Label lblFormat = new Label();
            lblFormat.Text = "Address:";
            lblFormat.Top = 60;
            lblFormat.Left = 5;
            lblFormat.Width = 70;
            pPanel.Controls.Add(lblFormat);

            cbComport = new ComboBox();
            foreach (string port in SerialPort.GetPortNames())
            {
                cbComport.Items.Add(port);
            }
            cbComport.SelectedIndex = cbComport.Items.IndexOf(comport);
            cbComport.Top = 5;
            cbComport.Left = 84;
            cbComport.Width = 368;
            pPanel.Controls.Add(cbComport);

            cbBaudrate = new ComboBox();
            cbBaudrate.Items.Add("19200");
            cbBaudrate.Items.Add("9600");
            cbBaudrate.Items.Add("4800");
            cbBaudrate.Items.Add("2400");
            cbBaudrate.Items.Add("1200");
            cbBaudrate.SelectedIndex = cbBaudrate.Items.IndexOf(baud.ToString());
            cbBaudrate.Top = 31;
            cbBaudrate.Left = 84;
            cbBaudrate.Width = 368;
            pPanel.Controls.Add(cbBaudrate);

            cbAddress = new ComboBox();
            for (int i = 0; i <= 30; i++)
            {
                cbAddress.Items.Add(i.ToString());
            }
            cbAddress.SelectedIndex = cbAddress.Items.IndexOf(address.ToString());
            cbAddress.Top = 57;
            cbAddress.Left = 84;
            cbAddress.Width = 368;
            pPanel.Controls.Add(cbAddress);

            Label lblHelp = new Label();
            lblHelp.Text = "To determine address and current baudrate\nPlease press the REM/LOC button for at least 3 sec.\nAddress is shown in voltage display and address in current display.\n\nPlease ensure that SW 6 is down (RS232 setting)";
            lblHelp.Top = 86;
            lblHelp.Left = 5;
            lblHelp.Width = 470;
            lblHelp.Height = 70;
            pPanel.Controls.Add(lblHelp);


            return 0;
        }


        public int AdvancedSettingsSetup(object panel)
        {
            throw new NotImplementedException();
        }

        public int GetOutputCount()
        {
            return devInfo.outputs;
        }


        public DeviceInformation GetDeviceInformation()
        {
            return devInfo;
        }


        public int GetMinimalUpdateTimeMS()
        {
            return 50; /* 50 ms update rate .. TOE is very slow */
        }
    }
}
