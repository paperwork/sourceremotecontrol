using System;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CheckPluginInterface;

namespace PluginCheckNet
{
	public class UdpState
	{
		public IPEndPoint e ;
		public UdpClient u ;
	}

	// State object for receiving data from remote device.
	public class TcpState {
	    // Client socket.
	    public Socket workSocket = null;
	    // Size of receive buffer.
	    public const int BufferSize = 2048;
	    // Receive buffer.
	    public byte[] buffer = new byte[BufferSize];
	    // Received data string.
	    public StringBuilder sb = new StringBuilder();
	}

	public class CheckPlugin:InterfaceCheckPlugin
    {

		private int Instance = 0;
		private ManualResetEvent messageSent = new ManualResetEvent(false);
		private ManualResetEvent messageReceive = new ManualResetEvent(false);
		private ManualResetEvent messageTCPConnect = new ManualResetEvent(false);
		private string messageReceivedData = "";
		private addTextCP addTextCB;
		private string confDir = "";

		/* Form components */
		private Form setupForm;
        private TextBox tbSendBeforeCheck;
        private TextBox tbCheck;
		private TextBox tbServerAddress;
		private RadioButton rbTCP;
		private RadioButton rbUDP;
		private NumericUpDown nudTimeout;
		private NumericUpDown nudPort;

		/* data values used by form components */
		private string optStrSendBeforeCheck;
		private string optStrCheck;
		private string optStrServerAddress;
		private string optStrMode;
		private int    optTimeout;
		private int    optPort;

        private int checkRunning = 0;

		public string GetPluginName() {
			return "NetCheckPlugin";
		}

        public void initCheckPlugin(int instance, addTextCP callback, string configDir) {
			this.addTextCB = callback;
			this.Instance = instance;
			this.confDir = configDir;

			try {
				 // Create Settings Table
                DataTable mySettings = new DataTable("SettingsPluginCheckNet");
                mySettings.ReadXml(confDir + Path.DirectorySeparatorChar + "config_plugin_checknet.xml");
                
                optStrServerAddress = mySettings.Rows[instance]["ServerAddress"].ToString();
                optPort = Convert.ToInt32(mySettings.Rows[instance]["Port"].ToString(), 10);
                optStrMode = mySettings.Rows[instance]["Mode"].ToString();
                optTimeout = Convert.ToInt32(mySettings.Rows[instance]["Timeout"].ToString(), 10);
                optStrCheck = mySettings.Rows[instance]["CheckStr"].ToString();
                optStrSendBeforeCheck = mySettings.Rows[instance]["SendBeforeCheckStr"].ToString();
			}
			catch {
				// loading failed -> go to defaults
				optStrCheck = "";
				optStrMode  = "TCP";
				optStrSendBeforeCheck  = "";
				optStrServerAddress = "127.0.0.1";
				optTimeout = 1000;
				optPort = 80;
			}
		}

		public void setupCheckPlugin()
        {
			setupForm = new Form();
            setupForm.Width = 420;
            setupForm.Height = 166;
            setupForm.FormBorderStyle = FormBorderStyle.FixedSingle;
            setupForm.MaximizeBox = false;
            setupForm.MinimizeBox = false;
			setupForm.Text = "Net Check Setup - Instance: " + this.Instance.ToString();
            setupForm.FormClosing += new FormClosingEventHandler(setupForm_Closing);

            tbSendBeforeCheck = new TextBox();
            tbSendBeforeCheck.Height = 20;
            tbSendBeforeCheck.Width = 300;
            tbSendBeforeCheck.Top = 12;
            tbSendBeforeCheck.Left = 110;
            tbSendBeforeCheck.Text = optStrSendBeforeCheck;
            setupForm.Controls.Add(tbSendBeforeCheck);

            tbCheck = new TextBox();
            tbCheck.Height = 20;
            tbCheck.Width = 300;
            tbCheck.Top = 38;
            tbCheck.Left = 110;
            tbCheck.Text = optStrCheck;
            setupForm.Controls.Add(tbCheck);

            Label lbl = new Label();
            lbl.Text = "Data to send before";
            lbl.Top = 15;
            lbl.Left = 5;
            lbl.Width = 101;
            lbl.Height = 13;
            setupForm.Controls.Add(lbl);

            lbl = new Label();
            lbl.Text = "Data to check for";
            lbl.Top = 41;
            lbl.Left = 5;
            lbl.Width = 101;
            lbl.Height = 13;
            setupForm.Controls.Add(lbl);

			lbl = new Label();
            lbl.Text = "Server Address";
            lbl.Top = 67;
            lbl.Left = 5;
            lbl.Width = 101;
            lbl.Height = 13;
            setupForm.Controls.Add(lbl);

			tbServerAddress = new TextBox();
            tbServerAddress.Height = 20;
            tbServerAddress.Width = 200;
            tbServerAddress.Top = 64;
            tbServerAddress.Left = 110;
            tbServerAddress.Text = optStrServerAddress;
            setupForm.Controls.Add(tbServerAddress);

			Panel pan = new Panel();
			pan.Top = 64;
			pan.Left = 310;
			pan.Width = 100;
			pan.Height = 20;

			rbUDP = new RadioButton();
			rbUDP.Top = 0;
			rbUDP.Left = 5;
			rbUDP.Width = 45;
			rbUDP.Height = 20;
			rbUDP.Text = "UDP";
			rbUDP.Checked  = (optStrMode == "UDP") ? true : false;

			rbTCP = new RadioButton();
			rbTCP.Top = 0;
			rbTCP.Left = 55;
			rbTCP.Width = 45;
			rbTCP.Height = 20;
			rbTCP.Text = "TCP";
			rbTCP.Checked  = (optStrMode == "TCP") ? true : false;

			pan.Controls.Add(rbUDP);
			pan.Controls.Add(rbTCP);
			setupForm.Controls.Add(pan);

			lbl = new Label();
            lbl.Text = "Port:";
            lbl.Top = 93;
            lbl.Left = 5;
            lbl.Width = 101;
            lbl.Height = 13;
            setupForm.Controls.Add(lbl);

			nudPort = new NumericUpDown();
            nudPort.Minimum = 1;
            nudPort.Maximum = 999999;
            nudPort.DecimalPlaces = 0;
            nudPort.Increment = 100;
			nudPort.Value = optPort;
            nudPort.Top = 90;
            nudPort.Left = 110;
            nudPort.Width = 300;
            setupForm.Controls.Add(nudPort);


			lbl = new Label();
            lbl.Text = "Timeout:";
            lbl.Top = 119;
            lbl.Left = 5;
            lbl.Width = 101;
            lbl.Height = 13;
            setupForm.Controls.Add(lbl);

			nudTimeout = new NumericUpDown();
            nudTimeout.Minimum = 10;
            nudTimeout.Maximum = 999999;
            nudTimeout.DecimalPlaces = 0;
            nudTimeout.Increment = 100;
            nudTimeout.Value = optTimeout;
            nudTimeout.Top = 116;
            nudTimeout.Left = 110;
            nudTimeout.Width = 300;
            setupForm.Controls.Add(nudTimeout);

			setupForm.ShowDialog();
		}

		private void setupForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
			addTextCB("Setup geschlossen\r\n");
			optStrCheck = tbCheck.Text;
            optStrSendBeforeCheck = tbSendBeforeCheck.Text;
			optStrMode = (rbTCP.Checked) ? "TCP" : "UDP";
			optStrServerAddress = tbServerAddress.Text;
			optPort = (int)nudPort.Value;
			optTimeout = (int)nudTimeout.Value;

			DataTable mySettings;
			try {
				 // Create Settings Table
                mySettings = new DataTable("SettingsPluginCheckNet");
                mySettings.ReadXml(confDir + Path.DirectorySeparatorChar + "config_plugin_checknet.xml");
			}
            catch 
            {
                /* file does not exists -> create one */
                // Create DataTable for Settings
				mySettings = new DataTable("SettingsPluginCheckNet");
                mySettings.Columns.Add("ServerAddress", typeof(string));
                mySettings.Columns.Add("Mode", typeof(string));
                mySettings.Columns.Add("Timeout", typeof(Int32));
                mySettings.Columns.Add("Port", typeof(Int32));
                mySettings.Columns.Add("CheckStr", typeof(string));
                mySettings.Columns.Add("SendBeforeCheckStr", typeof(string));
			}
			while(mySettings.Rows.Count <= this.Instance) {
                mySettings.Rows.Add(new object[6] { "127.0.0.1", "TCP", 1000, 80, "CheckString", "BeforeCheckString"});
            }

			mySettings.Rows[this.Instance]["ServerAddress"]      = optStrServerAddress;
			mySettings.Rows[this.Instance]["Mode"]               = optStrMode;
            mySettings.Rows[this.Instance]["Timeout"]            = optTimeout;
            mySettings.Rows[this.Instance]["Port"]               = optPort;
            mySettings.Rows[this.Instance]["CheckStr"]           = optStrCheck;
            mySettings.Rows[this.Instance]["SendBeforeCheckStr"] = optStrSendBeforeCheck;

            // Write to Isolated Storage
            mySettings.WriteXml(confDir + Path.DirectorySeparatorChar + "config_plugin_checknet.xml", XmlWriteMode.WriteSchema);
		}

		private void UDPSendCallback (IAsyncResult ar)
		{
            try
            {
                UdpClient u = (UdpClient)ar.AsyncState;
                u.EndSend(ar);
                messageSent.Set();
            }
            catch (Exception e)
            {
                if(checkRunning > 0) 
                    Console.WriteLine("UDP Send: " + e.ToString());
            }
		}

		private void UDPReceiveCallback (IAsyncResult ar)
		{
            try
            {
                UdpClient u = (UdpClient)((UdpState)(ar.AsyncState)).u;
                IPEndPoint e = (IPEndPoint)((UdpState)(ar.AsyncState)).e;

                Byte[] receiveBytes = u.EndReceive(ar, ref e);
                messageReceivedData = Encoding.ASCII.GetString(receiveBytes);

                messageReceive.Set();
            }
            catch (Exception e)
            {
                if (checkRunning > 0)
                    Console.WriteLine("UDP Receive: " + e.ToString());
            }
		}

		private void TCPConnectCallback (IAsyncResult ar) {
			try {
				Socket client = (Socket) ar.AsyncState;
				client.EndConnect(ar);
				messageTCPConnect.Set();
			}
			catch (Exception e) {
                if (checkRunning > 0)
				    Console.WriteLine("TCP Connect: " + e.ToString());
			}
		}

		private void TCPSendCallback (IAsyncResult ar) {
			try {
				Socket client = (Socket) ar.AsyncState;
				client.EndSend(ar);

				messageSent.Set();
			}
			catch (Exception e) {
                if (checkRunning > 0)
				    Console.WriteLine("TCP Send: " + e.ToString());
			}
		}

		private void TCPReceiveCallback( IAsyncResult ar ) {
	        try {
	            TcpState state = (TcpState) ar.AsyncState;
	            Socket client = state.workSocket;

	            // Read data from the remote device.
	            int bytesRead = client.EndReceive(ar);

	            if (bytesRead > 0) {
	                // There might be more data, so store the data received so far.
	            	state.sb.Append(Encoding.ASCII.GetString(state.buffer,0,bytesRead));

	                // Get the rest of the data.
	                client.BeginReceive(state.buffer,0,TcpState.BufferSize,0,
	                    new AsyncCallback(TCPReceiveCallback), state);
	            } else {
	                // All the data has arrived; put it in response.
	                if (state.sb.Length > 1) {
	                    messageReceivedData = state.sb.ToString();
	                }
	                // Signal that all bytes have been received.
	                messageReceive.Set();
	            }
	        } 
			catch (Exception e) {
                if (checkRunning > 0)
	                Console.WriteLine(e.ToString());
	        }
	    }

        /* return values 
         * 0 = check ok
         * 1 = check failed
         * 2 = check timeout 
         * -1 = could not run check */
        public int runCheck ()
		{
			string serverAddress = optStrServerAddress;
			int port = optPort;
			string mode = optStrMode;
			string checkString = optStrCheck;
			int result = 0;
			int timeout = optTimeout; /* ms */
			Byte[] sendBytes = new Byte[1024];
			int c = 0, i = 0;
            bool firstRemoteClose = true;

			// convert $xx chars to byte representation
			for (i=0, c=0; i < optStrSendBeforeCheck.Length; i++)
            {
                if (optStrSendBeforeCheck[i] == '$')
                {
                    i++;
                    if (optStrSendBeforeCheck[i] == '$') sendBytes[c++] = (byte)'$';
                    else
                    {
                        string str = optStrSendBeforeCheck.Substring(i, 2);
                        i++; 
                        sendBytes[c++] = (byte) Convert.ToByte(str, 16);
                    }
                }
                else sendBytes[c++] = (byte) optStrSendBeforeCheck[i];
            }

			Console.WriteLine ("NetCheckPlugin RunCheck with Instance: " + this.Instance.ToString ());
        beginRunCheck:
			try {
				// reset all wait events
            
				messageSent.Reset();
				messageReceive.Reset();
				messageTCPConnect.Reset();
				messageReceivedData = "";

                checkRunning = 1;

				if (mode == "UDP") {
					IPEndPoint RemoteIpEndPoint = new IPEndPoint (IPAddress.Any, 0);
					UdpClient client = new UdpClient (RemoteIpEndPoint);
					UdpState state = new UdpState ();
					state.e = RemoteIpEndPoint;
					state.u = client;

					client.BeginSend (sendBytes, c, serverAddress, port, new AsyncCallback (UDPSendCallback), client);
					if(!messageSent.WaitOne(timeout)) {
						Console.WriteLine ("UDP: Failed to send message");
						result = 2;
						goto runCheckEnd;
					}
					client.BeginReceive (new AsyncCallback (UDPReceiveCallback), state);
					if(!messageReceive.WaitOne(timeout)) {
						Console.WriteLine ("UDP: Failed to receive message");
						result = 2;
						goto runCheckEnd;
					}
					string tmp = "";
					for (i = 0; i < messageReceivedData.Length; i++)
					{
						if (Char.IsControl(messageReceivedData[i]))
						{
							int b = messageReceivedData[i];
							tmp += '$' + b.ToString("X2");
						}
						else tmp += messageReceivedData[i];
					}
					messageReceivedData = tmp;
				}
				else if(mode == "TCP") {
					Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
					client.BeginConnect(serverAddress, port, new AsyncCallback(TCPConnectCallback), client);
					if(!messageTCPConnect.WaitOne(timeout)) {
						Console.WriteLine ("TCP: Failed to connect");
                        addTextCB("TCP: Failed to connect\n");
                        result = 2;
						goto runCheckEnd;
					}

					client.BeginSend(sendBytes, 0, c, 0, new AsyncCallback(TCPSendCallback), client);
                    Thread.Sleep(500);
					if(!messageSent.WaitOne(timeout)) {
						Console.WriteLine ("TCP: Failed to send message");
                        addTextCB("TCP: Failed to send message\n");
                        result = 2;
						goto runCheckEnd;
					}
                    TcpState state = new TcpState();
					state.workSocket = client;
                    client.Blocking = true;
                    client.ReceiveTimeout = timeout;
					
                    int recvLen = client.Receive(state.buffer);
                    if (recvLen < 1) { 
						Console.WriteLine ("TCP: Failed to receive message"); 
                        addTextCB("TCP: Failed to receive message\n");
                        result = 2;
						goto runCheckEnd;
					}

                    char[] chars = Encoding.ASCII.GetChars(state.buffer);
                    for (i = 0; i < recvLen; i++)
                    {
                        if (Char.IsControl(chars[i]))
                        {
                            int b = chars[i];
                            messageReceivedData += '$' + b.ToString("X2");
                        }
                        else messageReceivedData += chars[i];
                    }
                    
                    

                    client.Shutdown(SocketShutdown.Both);
					client.Close();
				}
				else {
					/* some error occured, either TCP or UDP selected */
					result = -1;
				}
            } catch (System.Net.Sockets.SocketException e) {
                if ((uint)e.ErrorCode == 0x80004005 && firstRemoteClose == true)
                {
                    firstRemoteClose = false;
                    Console.WriteLine("Restart ... because remote close connection");
                    goto beginRunCheck;
                }
                else
                {
                    Console.WriteLine("NetCheck failed: " + e.ToString());
                    addTextCB("NetCheck failed: " + e.ToString());
                    result = 1;
                    goto runCheckEnd;
                }
			} catch (Exception e) {
                Console.WriteLine ("NetCheck failed: " + e.ToString ());
                addTextCB("NetCheck failed: " + e.ToString());
				result = 1;
                goto runCheckEnd;
			}
            if (String.Compare(messageReceivedData, 0, checkString, 0, checkString.Length) != 0) { 
				addTextCB("received data (" + messageReceivedData + ") does not match checkstring (" + checkString + ")\n");
				result = 1;
			}

        runCheckEnd:
            checkRunning = 0;
			return result;
		}
	}
}
