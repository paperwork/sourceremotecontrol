/*  SourceRemoteControl
    Copyright (C) 2013  Sebastian Block

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using System;
using System.IO;
using System.Data;
using System.Threading;
using System.Windows.Forms;
using FunctionPluginInterface;
using SourcePluginInterface;
using ClassDeviceInformation;

namespace PluginRamp611312
{
    public class FunctionPlugin : InterfaceFunctionPlugin
    {
		enum checksEnum {UeminEin, SDLavAus};
        delegate void btnSetEnableCallback(Button btn, bool enable);
        delegate void lblSetTextCallback(Label lbl, string text);

        private NumericUpDown nudVemin;
        private NumericUpDown nudSDLAV;
        private NumericUpDown nudtBetweenTests;
        private NumericUpDown nudOutput;
        private Button btnStop;
        private Button btnStart;
        private Label lblStatus;
        private Label lblVoltage;
        private CheckBox cbCheckSDLAVWaitTimeout;
        private CheckBox cbTestRun;
        private CheckBox cbTestStopWhenFailed;
        private ComboBox cbTestToRun;

        private InterfaceSourcePlugin curSource;

        private bool stopRamp = false;
        private string confDir;

        private addText addTextCallback;
        private runCheckFP runCheckCB;
		private functionDoneFP checkDoneCallback;

        public void initFunction(object panel, SourcePluginInterface.InterfaceSourcePlugin source, addText callback, runCheckFP runchkCB, functionDoneFP checkDoneCB, string configDir)
        {
            Panel pPanel = (Panel) panel;
            addTextCallback = callback;
            curSource = source;
            runCheckCB = runchkCB;
			checkDoneCallback = checkDoneCB;
            confDir = configDir;
            DeviceInformation devInfo = curSource.GetDeviceInformation();

            float minSpg, sdlavSpg;
            int output, tBetween;
            bool checkRun, checkStopFailed;
            bool checkSDLAVWaitTimeout;
            int testToRunIndex;
            /** 
             * load settings 
             */
            try
            {
                // Create Settings Table
                DataTable mySettings = new DataTable("SettingsPluginRamp61131-2");
                mySettings.ReadXml(confDir + Path.DirectorySeparatorChar + "config_plugin_ramp61131-2.xml");
                
                minSpg = (float)Convert.ToDecimal(mySettings.Rows[0]["VoltageStart"].ToString());
                sdlavSpg = (float)Convert.ToDecimal(mySettings.Rows[0]["VoltageStop"].ToString());
                output = Convert.ToInt32(mySettings.Rows[0]["Output"].ToString(), 10);
                checkRun = Convert.ToBoolean(mySettings.Rows[0]["RunCheck"].ToString());
                checkStopFailed = Convert.ToBoolean(mySettings.Rows[0]["StopOnFailedCheck"].ToString());
                checkSDLAVWaitTimeout = Convert.ToBoolean(mySettings.Rows[0]["CheckSDLAVWaitTimeout"].ToString());
				tBetween = Convert.ToInt32(mySettings.Rows[0]["DelayBetween"].ToString(), 10);
                testToRunIndex = Convert.ToInt32(mySettings.Rows[0]["TestToRunIndex"].ToString(), 10);
            }
            catch
            {
                minSpg = (float) 0.0;
                sdlavSpg = (float)24.0;
                output = 1;
                checkRun = false;
                checkStopFailed = false;
				tBetween = 5000;
                checkSDLAVWaitTimeout = true;
                testToRunIndex = 3;
            }
            
            /**
             * create for entries
             */
            Label lbl = new Label();
            lbl.Text = "Ue min [V]";
            lbl.Top = 10;
            lbl.Left = 5;
            lbl.Width = 85;
            lbl.Height = 13;
            pPanel.Controls.Add(lbl);

            nudVemin = new NumericUpDown();
            nudVemin.Left   = 195;
            nudVemin.Top    =  8;
            nudVemin.Width  = 74;
            nudVemin.Height = 20;
            nudVemin.DecimalPlaces = 2;
            nudVemin.Minimum = (decimal) devInfo.voltageMin;
            nudVemin.Maximum = (decimal) devInfo.voltageMax;
            if(minSpg >= devInfo.voltageMin && minSpg <= devInfo.voltageMax)
                nudVemin.Value = (decimal) minSpg;
            else
                nudVemin.Value = (decimal)devInfo.voltageMin;
            pPanel.Controls.Add(nudVemin);

            lbl = new Label();
            lbl.Text = "0.9 SDL AV[V]";
            lbl.Top = 36;
            lbl.Left = 5;
            lbl.Width = 80;
            lbl.Height = 13;
            pPanel.Controls.Add(lbl);

            nudSDLAV = new NumericUpDown();
            nudSDLAV.Left = 195;
            nudSDLAV.Top = 34;
            nudSDLAV.Width = 74;
            nudSDLAV.Height = 20;
            nudSDLAV.DecimalPlaces = 2;
            nudSDLAV.Minimum = (decimal)devInfo.voltageMin;
            nudSDLAV.Maximum = (decimal)devInfo.voltageMax;
            if (sdlavSpg >= devInfo.voltageMin && sdlavSpg <= devInfo.voltageMax)
                nudSDLAV.Value = (decimal)sdlavSpg;
            else
                nudSDLAV.Value = (decimal)devInfo.voltageMax;
            pPanel.Controls.Add(nudSDLAV);

            lbl = new Label();
            lbl.Text = "Time between tests [ms]";
            lbl.Top = 62;
            lbl.Left = 5;
            lbl.Width = 61;
            lbl.Height = 13;
            pPanel.Controls.Add(lbl);

            nudtBetweenTests = new NumericUpDown();
            nudtBetweenTests.Left = 195;
            nudtBetweenTests.Top = 60;
            nudtBetweenTests.Width = 74;
            nudtBetweenTests.Height = 20;
			nudtBetweenTests.Minimum = 1000;//curSource.GetMinimalUpdateTimeMS();
            nudtBetweenTests.Maximum = 10000;
            if (tBetween >= nudtBetweenTests.Minimum && tBetween <= nudtBetweenTests.Maximum)
                nudtBetweenTests.Value = tBetween;
            else
                nudtBetweenTests.Value = nudtBetweenTests.Minimum;
            pPanel.Controls.Add(nudtBetweenTests);

			lbl = new Label();
            lbl.Text = "Ausgang";
            lbl.Top = 88;
            lbl.Left = 5;
            lbl.Width = 80;
            lbl.Height = 13;
            pPanel.Controls.Add(lbl);

            nudOutput = new NumericUpDown();
            nudOutput.Left = 195;
            nudOutput.Top = 86;
            nudOutput.Width = 74;
            nudOutput.Height = 20;
            nudOutput.Minimum = 1;
            nudOutput.Maximum = curSource.GetOutputCount();
            if (output >= nudOutput.Minimum && output <= nudOutput.Maximum)
                nudOutput.Value = output;
            else
                nudOutput.Value = nudOutput.Minimum;
            pPanel.Controls.Add(nudOutput);

            lbl = new Label();
            lbl.Text = "Test to run";
            lbl.Top = 114;
            lbl.Left = 5;
            lbl.Width = 80;
            lbl.Height = 13;
            pPanel.Controls.Add(lbl);

            cbTestToRun = new ComboBox();
            cbTestToRun.Left = 195;
            cbTestToRun.Top = 112;
            cbTestToRun.Width = 100;
            cbTestToRun.Height = 20;
            cbTestToRun.Items.Add("6.4.2.1");
            cbTestToRun.Items.Add("6.4.2.2 (fast)");
            cbTestToRun.Items.Add("6.4.2.2 (slow)");
            cbTestToRun.Items.Add("all");
            cbTestToRun.SelectedIndex = testToRunIndex;
            pPanel.Controls.Add(cbTestToRun);

            cbCheckSDLAVWaitTimeout = new CheckBox();
            cbCheckSDLAVWaitTimeout.Left = 5;
            cbCheckSDLAVWaitTimeout.Top = 140;
            cbCheckSDLAVWaitTimeout.Width = 240;
            cbCheckSDLAVWaitTimeout.Text = "Check SDLAV - Wait for timeout";
            cbCheckSDLAVWaitTimeout.Checked = checkSDLAVWaitTimeout;
            pPanel.Controls.Add(cbCheckSDLAVWaitTimeout);

            cbTestRun = new CheckBox();
            cbTestRun.Left = 5;
            cbTestRun.Top = 166;
            cbTestRun.Width = 180;
            cbTestRun.Text = "Run Checks";
            cbTestRun.Checked = checkRun;
            pPanel.Controls.Add(cbTestRun);

            cbTestStopWhenFailed = new CheckBox();
            cbTestStopWhenFailed.Left = 195;
            cbTestStopWhenFailed.Top = 166;
            cbTestStopWhenFailed.Width = 120;
            cbTestStopWhenFailed.Text = "Stop on error";
            cbTestStopWhenFailed.Checked = checkStopFailed;
            pPanel.Controls.Add(cbTestStopWhenFailed);
            
            btnStart = new Button();
            btnStart.Left = 5;
            btnStart.Top = 192;
            btnStart.Width = 75;
            btnStart.Height = 23;
            btnStart.Text = "Start";
            btnStart.Click += new System.EventHandler(btnStart_Click);
            pPanel.Controls.Add(btnStart);

            btnStop = new Button();
            btnStop.Left = 90;
            btnStop.Top = 192;
            btnStop.Width = 75;
            btnStop.Height = 23;
            btnStop.Text = "Stop";
            btnStop.Enabled = false;
            btnStop.Click += new System.EventHandler(btnStop_Click);
            pPanel.Controls.Add(btnStop);

            lblStatus = new Label();
            lblStatus.Text = "Status:";
            lblStatus.Top = 218;
            lblStatus.Left = 5;
            lblStatus.Width = 280;
            lblStatus.Height = 13;
            pPanel.Controls.Add(lblStatus);

            lblVoltage = new Label();
            lblVoltage.Text = "Voltage:";
            lblVoltage.Top = 244;
            lblVoltage.Left = 5;
            lblVoltage.Width = 280;
            lblVoltage.Height = 13;
            pPanel.Controls.Add(lblVoltage);
        }


        public string getPluginName()
        {
            return "Tests nach 61131-2";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (curSource == null)
            {
                lblSetText(lblStatus, "Nicht mit Quelle verbunden!!\r\n");
                return;
            }
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            stopRamp = false;
            new Thread(delay).Start();

            /** speichere Einstellungen */
            DataTable mySettings;
            
            // Create DataTable for Settings
            mySettings = new DataTable("SettingsPluginRamp61131-2");
            mySettings.Columns.Add("VoltageStart", typeof(decimal));
            mySettings.Columns.Add("VoltageStop", typeof(decimal));
            mySettings.Columns.Add("DelayBetween", typeof(Int32));
            mySettings.Columns.Add("Output", typeof(Int32));
            mySettings.Columns.Add("RunCheck", typeof(Boolean));
            mySettings.Columns.Add("StopOnFailedCheck", typeof(Boolean));
            mySettings.Columns.Add("CheckSDLAVWaitTimeout", typeof(Boolean));
            mySettings.Columns.Add("TestToRunIndex", typeof(Int32));

            mySettings.Rows.Add(new object[8] { 
                nudVemin.Value,
                nudSDLAV.Value,
                nudtBetweenTests.Value,
                nudOutput.Value,
                cbTestRun.Checked,
                cbTestStopWhenFailed.Checked,
                cbCheckSDLAVWaitTimeout.Checked,
                cbTestToRun.SelectedIndex
            });

            mySettings.WriteXml(confDir + Path.DirectorySeparatorChar + "config_plugin_ramp61131-2.xml", XmlWriteMode.WriteSchema);

            
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            stopRamp = true;
        }

		private void sleepThread(int ms)
		{
			if(ms < 500) System.Threading.Thread.Sleep(ms);
			else 
			{
				while(ms >= 500 && !stopRamp) {
					ms -= 500;
					System.Threading.Thread.Sleep(500);
				}
				if(!stopRamp && ms>0) 
					System.Threading.Thread.Sleep(ms);
			}
		}

		private void delay()
        {
            float vStart = (float)nudVemin.Value;
            float vStop = (float)nudSDLAV.Value;
            float vStep = 0.0f;
            int tBetweenTests = (int)nudtBetweenTests.Value;
			int tStep = curSource.GetMinimalUpdateTimeMS();
            int output = (int)nudOutput.Value;
            float f = (float)0.0;
            float vBefore = curSource.GetVoltage(output);
            int i = 0;
			int cycles = 0;
			int result = 0;
            int checkSDLAVTestResult = 0;
            int testToRun = cbTestToRun.SelectedIndex;

            if (cbCheckSDLAVWaitTimeout.Checked) checkSDLAVTestResult = 2;

            if (testToRun == 0 || testToRun == 3)
            {
                // Test allmähliches Ab-/Zuschalten 61131-2 6.4.2.1
                // Parameter 60 s Rampenzeit
                // von Uemin auf 0 
                // Verweilzeit bei 0 V: 10 s
                // 3 Zylken
                cycles = 3;
                vStart = (float)nudVemin.Value;
                vStop = 0.0f;
                tStep = (int)curSource.GetMinimalUpdateTimeMS();
                vStep = vStart / (60000 / tStep);

                lblSetText(lblStatus, "Starte Rampe mit folgenden Einstellungen\r\nStart-Spannung: " + vStart.ToString("F2") + "V\r\nStop-Spannung: " + vStop.ToString("F2") + "V\r\nVStep: " + vStep.ToString("F2") + "V\r\n");

                while (cycles-- > 0 && !stopRamp)
                {
                    i++;
                    lblSetText(lblStatus, "Laufe Rampe: " + i.ToString());
                    addTextCallback("Laufe Rampe: " + i.ToString() + "\r\n");

                    f = vStart;
                    // Setze Uemin
                    curSource.SetVoltage(output, vStart);
                    // warte minimale Zeit vor dem Test
                    sleepThread(tBetweenTests);

                    // Test durchführen ob Device ok
                    if (stopRamp == false && cbTestRun.Checked)
                    {
                        if (runCheckCB((int)checksEnum.UeminEin) != 0 && cbTestStopWhenFailed.Checked && stopRamp == false)
                        {
                            addTextCallback("Überprüfung nach 61131-2 6.4.2.1 vor der Spannungsunterbrechung fehlgeschlagen...stoppe Rampe\r\n");
                            result = 1;
                            stopRamp = true;
                        }
                    }

                    while (Math.Abs(f - vStop) > (vStep - 0.000002) && !stopRamp)
                    {
                        curSource.SetVoltage(output, f);
                        lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                        if (vStart < vStop) f += vStep;
                        else f -= vStep;
                        sleepThread(tStep);
                    }

                    f = vStop;
                    curSource.SetVoltage(output, vStop);
                    lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                    sleepThread(10000);
                    // Test durchführen ob Device aus
                    if (stopRamp == false && cbTestRun.Checked)
                    {
                        if (runCheckCB((int)checksEnum.SDLavAus) != checkSDLAVTestResult && cbTestStopWhenFailed.Checked && stopRamp == false)
                        {
                            addTextCallback("Überprüfung nach 61131-2 6.4.2.1 auf ausgeschaltetes Gerät fehlgeschlagen...stoppe Rampe\r\n");
                            result = 2;
                            stopRamp = true;
                        }
                    }

                    while (Math.Abs(f - vStart) > (vStep - 0.000002) && !stopRamp)
                    {
                        curSource.SetVoltage(output, f);
                        lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                        if (vStart < vStop) f -= vStep;
                        else f += vStep;
                        sleepThread(tStep);
                    }

                    f = vStart;
                    curSource.SetVoltage(output, f);
                    lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");

                    // Test durchführen ob Device ok
                    if (stopRamp == false && cbTestRun.Checked)
                    {
                        if (runCheckCB((int)checksEnum.UeminEin) != 0 && cbTestStopWhenFailed.Checked && stopRamp == false)
                        {
                            addTextCallback("Überprüfung nach 61131-2 6.4.2.1 nach der Spannungsunterbrechung fehlgeschlagen...stoppe Rampe\r\n");
                            result = 3;
                            stopRamp = true;
                        }
                    }
                }
            }

            if (testToRun == 1 || testToRun == 3)
            {
                // Test Schnelle Änderung 61131-2 6.4.2.2
                // Parameter 5s Rampenzeit 
                // von Uemin auf 0 V 
                // 3 Zyklen
                cycles = 3;
                vStart = (float)nudVemin.Value;
                vStop = 0.0f;
                tStep = (int)curSource.GetMinimalUpdateTimeMS();
                vStep = vStart / (5000 / tStep);

                lblSetText(lblStatus, "Starte Rampe mit folgenden Einstellungen\r\nStart-Spannung: " + vStart.ToString("F2") + "V\r\nStop-Spannung: " + vStop.ToString("F2") + "V\r\nVStep: " + vStep.ToString("F2") + "V\r\n");

                while (cycles-- > 0 && !stopRamp)
                {
                    i++;
                    lblSetText(lblStatus, "Laufe Rampe: " + i.ToString());
                    addTextCallback("Laufe Rampe: " + i.ToString() + "\r\n");

                    f = vStart;
                    // Setze Uemin
                    curSource.SetVoltage(output, vStart);
                    // warte minimale Zeit vor dem Test
                    sleepThread(tBetweenTests / 2);

                    // Test durchführen ob Device ok
                    if (stopRamp == false && cbTestRun.Checked)
                    {
                        if (runCheckCB((int)checksEnum.UeminEin) != 0 && cbTestStopWhenFailed.Checked && stopRamp == false)
                        {
                            addTextCallback("Überprüfung nach 61131-2 6.4.2.2 (schnell) vor der Spannungsunterbrechung fehlgeschlagen...stoppe Rampe\r\n");
                            result = 4;
                            stopRamp = true;
                        }
                    }

                    while (Math.Abs(f - vStop) > (vStep - 0.000002) && !stopRamp)
                    {
                        curSource.SetVoltage(output, f);
                        lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                        if (vStart < vStop) f += vStep;
                        else f -= vStep;
                        sleepThread(tStep);
                    }

                    f = vStop;
                    curSource.SetVoltage(output, vStop);
                    lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                    sleepThread(tStep);
                    // Test durchführen ob Device aus
                    if (stopRamp == false && cbTestRun.Checked)
                    {
                        if (runCheckCB((int)checksEnum.SDLavAus) != checkSDLAVTestResult && cbTestStopWhenFailed.Checked && stopRamp == false)
                        {
                            addTextCallback("Überprüfung nach 61131-2 6.4.2.2 (schnell) auf ausgeschaltetes Gerät fehlgeschlagen...stoppe Rampe\r\n");
                            result = 5;
                            stopRamp = true;
                        }
                    }

                    while (Math.Abs(f - vStart) > (vStep - 0.000002) && !stopRamp)
                    {
                        curSource.SetVoltage(output, f);
                        lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                        if (vStart < vStop) f -= vStep;
                        else f += vStep;
                        sleepThread(tStep);
                    }

                    if (stopRamp == false)
                    {
                        f = vStart;
                        curSource.SetVoltage(output, f);
                        lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                        // warte minimale Zeit vor dem Test
                        sleepThread(tBetweenTests / 2);
                    }

                    // Test durchführen ob Device ok
                    if (stopRamp == false && cbTestRun.Checked)
                    {
                        if (runCheckCB((int)checksEnum.UeminEin) != 0 && cbTestStopWhenFailed.Checked && stopRamp == false)
                        {
                            addTextCallback("Überprüfung nach 61131-2 6.4.2.2 (schnell) nach der Spannungsunterbrechung fehlgeschlagen...stoppe Rampe\r\n");
                            result = 6;
                            stopRamp = true;
                        }
                    }
                }
            }

            if (testToRun == 2 || testToRun == 3)
            {
                // Test Langsame Änderung 61131-2 6.4.2.2
                // Parameter 60s Rampenzeit 
                // von Uemin auf SDLAV 
                // 3 Zyklen
                cycles = 3;
                vStart = (float)nudVemin.Value;
                vStop = (float)nudSDLAV.Value;
                tStep = (int)curSource.GetMinimalUpdateTimeMS();
                vStep = (vStart - vStop) / (60000 / tStep);

                lblSetText(lblStatus, "Starte Rampe mit folgenden Einstellungen\r\nStart-Spannung: " + vStart.ToString("F2") + "V\r\nStop-Spannung: " + vStop.ToString("F2") + "V\r\nVStep: " + vStep.ToString("F2") + "V\r\n");

                while (cycles-- > 0 && !stopRamp)
                {
                    i++;
                    lblSetText(lblStatus, "Laufe Rampe: " + i.ToString());
                    addTextCallback("Laufe Rampe: " + i.ToString() + "\r\n");

                    f = vStart;
                    // Setze Uemin
                    curSource.SetVoltage(output, vStart);
                    // warte minimale Zeit vor dem Test
                    sleepThread(tBetweenTests / 2);

                    // Test durchführen ob Device ok
                    if (stopRamp == false && cbTestRun.Checked)
                    {
                        if (runCheckCB((int)checksEnum.UeminEin) != 0 && cbTestStopWhenFailed.Checked && stopRamp == false)
                        {
                            addTextCallback("Überprüfung nach 61131-2 6.4.2.2 (langsam) vor der Spannungsunterbrechung fehlgeschlagen...stoppe Rampe\r\n");
                            result = 7;
                            stopRamp = true;
                        }
                    }

                    while (Math.Abs(f - vStop) > (vStep - 0.000002) && !stopRamp)
                    {
                        curSource.SetVoltage(output, f);
                        lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                        if (vStart < vStop) f += vStep;
                        else f -= vStep;
                        sleepThread(tStep);
                    }

                    if (stopRamp == false)
                    {
                        f = vStop;
                        curSource.SetVoltage(output, vStop);
                        lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                        sleepThread(tStep);
                    }
                    // Test durchführen ob Device aus
                    if (stopRamp == false && cbTestRun.Checked)
                    {
                        if (runCheckCB((int)checksEnum.SDLavAus) != checkSDLAVTestResult && cbTestStopWhenFailed.Checked && stopRamp == false)
                        {
                            addTextCallback("Überprüfung nach 61131-2 6.4.2.2 (langsam) auf ausgeschaltetes Gerät fehlgeschlagen...stoppe Rampe\r\n");
                            result = 8;
                            stopRamp = true;
                        }
                    }

                    while (Math.Abs(f - vStart) > (vStep - 0.000002) && !stopRamp)
                    {
                        curSource.SetVoltage(output, f);
                        lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                        if (vStart < vStop) f -= vStep;
                        else f += vStep;
                        sleepThread(tStep);
                    }

                    if (stopRamp == false)
                    {
                        f = vStart;
                        curSource.SetVoltage(output, f);
                        lblSetText(lblVoltage, "Aktuelle Spannung: " + f.ToString("F2") + " V");
                        sleepThread(tBetweenTests / 2);
                    }

                    // Test durchführen ob Device ok
                    if (stopRamp == false && cbTestRun.Checked)
                    {
                        if (runCheckCB((int)checksEnum.UeminEin) != 0 && cbTestStopWhenFailed.Checked && stopRamp == false)
                        {
                            addTextCallback("Überprüfung nach 61131-2 6.4.2.2 (langsam) nach der Spannungsunterbrechung fehlgeschlagen...stoppe Rampe\r\n");
                            result = 9;
                            stopRamp = true;
                        }
                    }
                }
            }
            curSource.SetVoltage(output, vBefore);
            lblSetText(lblVoltage, "Aktuelle Spannung: " + vBefore.ToString("F2") + " V");
            btnSetEnable(btnStart, true);
            btnSetEnable(btnStop, false);

			checkDoneCallback(result);
        }

        private void lblSetText(Label lbl, string text)
        {
            if (lbl.InvokeRequired)
            {
                lblSetTextCallback d = new lblSetTextCallback(lblSetText);
                lbl.BeginInvoke(d, new object[] { lbl, text });
            }
            else
            {
                lbl.Text = text;
            }
        }

        private void btnSetEnable(Button btn, bool enable)
        {
            if (btn.InvokeRequired)
            {
                btnSetEnableCallback d = new btnSetEnableCallback(btnSetEnable);
                btn.BeginInvoke(d, new object[] { btn, enable });
            }
            else
            {
                btn.Enabled = enable;
            }
        }

        public void stopFromApplication()
        {
            stopRamp = true;
        }

		public void startFromApplication() 
		{
			btnStart_Click(this, null);
		}

		public int getCheckpointCount() {
			return 2;
		}

		public string[] getCheckPointNames() {
			string[] strlist = new string[getCheckpointCount()];

			strlist[(int)checksEnum.UeminEin] = "Spannung = Uemin (ein)";
			strlist[(int)checksEnum.SDLavAus] = "Spannung <= SDLav (aus)";

			return strlist;
		}
    }
}
