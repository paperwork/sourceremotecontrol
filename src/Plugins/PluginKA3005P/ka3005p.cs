﻿/*  SourceRemoteControl
    Copyright (C) 2016  Sebastian Block

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using System;
using System.Data;
using SourcePluginInterface;
using System.IO;
using System.IO.Ports;
using System.Globalization;
using System.Windows.Forms;
using ClassDeviceInformation;

namespace PluginKP3005
{
    public class SourcePlugin : InterfaceSourcePlugin
    {
        private SerialPort serialPort;
        private float lastVoltages = (float)0.0;
        private ComboBox cbComport = null;
        private DeviceInformation devInfo = new DeviceInformation();
        private string configDir;

        /* Konstanten */
        const float F_OUTPUT_INVALID = (float)-10010.0;
        const int I_OUTPUT_INVALID = -10;

        public int Connect(string connectionInformation)
        {
            string s;
            if (cbComport == null) return 1;
            if (serialPort != null)
            {
                if (serialPort.IsOpen) serialPort.Close();
                if (serialPort.PortName != cbComport.SelectedText) serialPort.PortName = cbComport.SelectedItem.ToString();
            }
            else
            {
                //tbOut.AppendText("Open Port: " + connectionData + "\n\r");
                serialPort = new SerialPort(cbComport.SelectedItem.ToString());
                //serialPort.DataReceived += new SerialDataReceivedEventHandler(SerialDataRecvHandler);
            }

            serialPort.StopBits = StopBits.One;
            serialPort.Parity = Parity.None;
            serialPort.DataBits = 8;
            serialPort.Handshake = Handshake.None;
            serialPort.BaudRate = 9600;

            serialPort.ReadTimeout = 500;

            serialPort.Open();
            s = SendRecv("*IDN?", 17);
            if (s.Contains("KA3005P"))
            {
                KP3005getDeviceInformation();
                lastVoltages = GetVoltage(1);
                
                /** speichere Einstellungen */
                DataTable mySettings;

                // Create DataTable for Settings
                mySettings = new DataTable("SettingsPluginKA3005P");
                mySettings.Columns.Add("ComPort", typeof(string));

                mySettings.Rows.Add(new object[1] { 
					cbComport.SelectedItem.ToString()
				});

                mySettings.WriteXml(configDir + Path.DirectorySeparatorChar + "config_plugin_ka3005p.xml", XmlWriteMode.WriteSchema);

            }
            else
            {
                serialPort.Close();
                return 1;
            }
            return 0;
        }

        public int Disconnect()
        {
            if (serialPort == null) return 1;
            if (serialPort.IsOpen)
            {
                serialPort.Close();
            }
            return 0;
        }

        public int EnableOutput(int output, bool value)
        {
            if (output > devInfo.outputs || output < 1) return I_OUTPUT_INVALID;
            return EnableOutputs(value);
        }

        public int EnableOutputs(bool value)
        {
            if (value)
                SendCmd("OUT1");
            else
                SendCmd("OUT0");
            return 0;
        }

        public float GetCurrentLimit(int output)
        {
            if (output > devInfo.outputs || output < 1) return F_OUTPUT_INVALID;

            string s = SendRecv("ISET1?", 5);
            return float.Parse(s, CultureInfo.InvariantCulture);
        }

        public int SetCurrentLimit(int output, float current)
        {
            string s = "ISET1:" + current.ToString("F1", CultureInfo.InvariantCulture);
            SendCmd(s);
            return 0;
        }

        public string GetPluginDate()
        {
            return "2016-10-05 11:41";
        }

        public string GetPluginName()
        {
            return "KA3005P";
        }

        public int GetPluginVersion()
        {
            throw new NotImplementedException();
        }

        public float GetVoltage(int output)
        {
            if (output > devInfo.outputs || output < 1) return F_OUTPUT_INVALID;
            
            string s = SendRecv("VSET1?", 5);
            return float.Parse(s, CultureInfo.InvariantCulture);
        }

        public int SetVoltage(int output, float voltage)
        {
            if (output > devInfo.outputs || output < 1) return I_OUTPUT_INVALID;
            string s = "VSET1:" + voltage.ToString("F2", CultureInfo.InvariantCulture);
            if (voltage >= 0) lastVoltages = voltage;
            SendCmd(s);
            return 0;
        }

        public bool isOutputEnabled(int output)
        {
            string s = SendRecv("STATUS?", 1);
            if ((s[0] & 0x40) == 0x40)
            {
                return true;
            }
            return false;
        }

        private string SendRecv(string cmd, int expected_length)
        {
            int i = 0;
            int readBytes = 0;
            byte[] buf = new byte[1024];
            string s = "";
            if (serialPort.IsOpen == false) return s;

            System.Text.Encoding.ASCII.GetBytes(cmd, 0, cmd.Length, buf, 0);
            serialPort.Write(buf, 0, cmd.Length);

            i = 0;
            buf[0] = 0;
            try
            {
                do
                {
                    readBytes = serialPort.Read(buf, i, 1);
                    if (readBytes > 0)
                    {
                        i += readBytes;
                    }
                } while (expected_length > i);
                s = System.Text.Encoding.ASCII.GetString(buf, 0, i);
            }
            catch (TimeoutException)
            {
                s = "timeout waiting for device";
            }
            //System.Threading.Thread.Sleep(50);
            return s;
        }

        private void SendCmd(string s)
        {
            //int counter = 10;
            if (serialPort.IsOpen)
            {
                serialPort.Write(s);
            }
            //while (counter-- > 0) System.Threading.Thread.Sleep(10);
        }

        private void KP3005getDeviceInformation()
        {
            string s = SendRecv("*IDN?", 17);
            devInfo.producer = s.Substring(0,5);
            devInfo.type = s.Substring(5,7);
            devInfo.serialNo = "unknown";
            devInfo.softwareRevision = s.Substring(12).Trim();

            /* min/max - Werte abfragen */
            devInfo.currentLimitMin = devInfo.currentMin = (float) 0;
            devInfo.currentLimitMax = devInfo.currentMax = (float) 5;
            devInfo.voltageLimitMin = devInfo.voltageMin = (float) 0;
            devInfo.voltageLimitMax = devInfo.voltageMax = (float) 30;
            devInfo.voltageOVPMin = (float)0;
            devInfo.voltageOVPMax = (float)30;
            devInfo.outputs = 1;

            devInfo.singleTurnOnOff = false;
        }

        public int ConnectionSettingSetup(object panel, string confDir)
        {
            Panel pPanel = (Panel)panel;
            configDir = confDir;

            string comport;
            /** 
            * load settings  
            */
            try
            {
                // Create Settings Table
                DataTable mySettings = new DataTable("SettingsPluginKA3005P");
                mySettings.ReadXml(confDir + Path.DirectorySeparatorChar + "config_plugin_ka3005p.xml");

                comport = mySettings.Rows[0]["ComPort"].ToString();
            }
            catch
            {
                comport = "COM1";
            }

            Label lblComport = new Label();
            lblComport.Text = "Comport:";
            lblComport.Top = 8;
            lblComport.Left = 5;
            lblComport.Width = 70;
            pPanel.Controls.Add(lblComport);

            cbComport = new ComboBox();
            foreach (string port in SerialPort.GetPortNames())
            {
                cbComport.Items.Add(port);
            }
            cbComport.SelectedIndex = cbComport.Items.IndexOf(comport);
            cbComport.Top = 5;
            cbComport.Left = 84;
            cbComport.Width = 368;
            pPanel.Controls.Add(cbComport);

            return 0;
        }


        public int AdvancedSettingsSetup(object panel)
        {
            throw new NotImplementedException();
        }

        public int GetOutputCount()
        {
            return devInfo.outputs;
        }


        public DeviceInformation GetDeviceInformation()
        {
            return devInfo;
        }


        public int GetMinimalUpdateTimeMS()
        {
            return 50; /* 50 ms update rate .. TOE is very slow */
        }
    }
}
