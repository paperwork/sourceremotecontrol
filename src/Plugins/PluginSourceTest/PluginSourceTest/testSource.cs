/*  SourceRemoteControl
    Copyright (C) 2013  Sebastian Block

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using System;
using System.Data;
using SourcePluginInterface;
using System.IO;
using System.IO.Ports;
using System.Globalization;
using System.Windows.Forms;
using ClassDeviceInformation;

namespace PluginTestSource
{
    public class SourcePlugin : InterfaceSourcePlugin
    {
        private int lastOutput = 0;
        private float[] lastVoltages = null;
		private float[] currents = null;
		private bool[] outputsActive = null;
        private ComboBox cbComport  = null;
        private ComboBox cbBaudrate = null;
        private ComboBox cbFormat   = null;
        private ComboBox cbStopBits = null;
        private ComboBox cbProtocol = null;
        private DeviceInformation devInfo = new DeviceInformation();
        private string configDir;

        /* Konstanten */
        const float F_OUTPUT_INVALID = (float) -10010.0;
        const int I_OUTPUT_INVALID = -10;

		private Boolean connected = false;

        public int Connect(string connectionInformation)
        {
            devInfo.producer = "Sebastian Block";
			devInfo.type = "Source Test";
			devInfo.serialNo = "noSerial";
			devInfo.softwareRevision = "1";
			devInfo.outputs = 4;
			devInfo.currentMin = 0f;
			devInfo.currentMax = 10f;
			devInfo.currentLimitMin = 0f;
			devInfo.currentLimitMax = 10f;
			devInfo.voltageMin = 0f;
			devInfo.voltageMax = 60f;
			devInfo.voltageLimitMin = 0f;
			devInfo.voltageLimitMax = 60f;
			devInfo.voltageOVPMin = 0f;
			devInfo.voltageOVPMax = 70f;
			devInfo.singleTurnOnOff = true;

			lastVoltages = new float[4];
			currents = new float[4];
			outputsActive = new bool[4];

#if false
                lastVoltages[0] = GetVoltage(1);
                lastVoltages[1] = GetVoltage(2);


                /** speichere Einstellungen */
                DataTable mySettings;

                // Create DataTable for Settings
                mySettings = new DataTable("SettingsPluginTOE895x");
                mySettings.Columns.Add("ComPort", typeof(string));
                mySettings.Columns.Add("Baudrate", typeof(Int32));
                mySettings.Columns.Add("FormatIndex", typeof(Int32));
                mySettings.Columns.Add("StopBitsIndex", typeof(Int32));
                mySettings.Columns.Add("ProtocolIndex", typeof(Int32));
            
                mySettings.Rows.Add(new object[5] { 
                    cbComport.SelectedItem.ToString(),
                    Int32.Parse(cbBaudrate.SelectedItem.ToString()),
                    cbFormat.SelectedIndex,
                    cbStopBits.SelectedIndex,
                    cbProtocol.SelectedIndex
                });

                mySettings.WriteXml(configDir + Path.DirectorySeparatorChar + "config_plugin_toe895x.xml", XmlWriteMode.WriteSchema);
#endif
			if(connected) Console.WriteLine("TestSource is already connected");
			connected = true;
			Console.WriteLine("TestSource connect");
            return 0;
        }

        public int Disconnect()
        {
			connected = false;
			Console.WriteLine("TestSource disconnect");
            return 0;
        }

        public int EnableOutput(int output, bool value)
        {
            if (output > devInfo.outputs || output < 1) return I_OUTPUT_INVALID;
            if (lastOutput != output)
            {
				lastOutput = output;
            }
			outputsActive[lastOutput-1] = value;
			if(value) Console.WriteLine("TestSource output {0} enabled", lastOutput);
			else Console.WriteLine("TestSource output {0} disabled", lastOutput);
            return 0;
        }

        public int EnableOutputs(bool value)
        {
            if (value) 
				Console.WriteLine("TestSource enable all outputs");
            else
				Console.WriteLine("TestSource disable all outputs");
            return 0;
        }

        public float GetCurrentLimit(int output)
        {
            if (output > devInfo.outputs || output < 1) return F_OUTPUT_INVALID;
            if (lastOutput != output)
            {
                lastOutput = output;
            }
            return currents[lastOutput-1];
        }

        public string GetPluginDate()
        {
            return "2013-12-02 20:30";
        }

        public string GetPluginName()
        {
            return "TestSource";
        }

        public int GetPluginVersion()
        {
            return 1;
        }

        public float GetVoltage(int output)
        {
            if (output > devInfo.outputs || output < 1) return F_OUTPUT_INVALID;
            if (lastOutput != output)
            {
                lastOutput = output;
            }
            return lastVoltages[lastOutput-1];
        }

        public int SetCurrentLimit(int output, float current)
        {
            if (output > devInfo.outputs || output < 1) return I_OUTPUT_INVALID;
            if (lastOutput != output)
            {
                lastOutput = output;
            }
			currents[lastOutput-1] = current;
			Console.WriteLine("TestSource output {0} current set {1}", output, current);
            return 0;
        }

        public int SetVoltage(int output, float voltage)
        {
            if (output > devInfo.outputs || output < 1) return I_OUTPUT_INVALID;
            if (lastOutput != output)
            {
                lastOutput = output;
            }
            if (voltage > 0) lastVoltages[(output - 1)] = voltage;
			Console.WriteLine("TestSource output {0} voltage set {1}", output, voltage);
            return 0;
        }

        public bool isOutputEnabled(int output)
        {
            if (output == -1) /* select all */
            {
                return true;
            }
            else
				return outputsActive[output-1];
        }
        
        public int ConnectionSettingSetup(object panel, string confDir)
        {
            Panel pPanel = (Panel)panel;

#if false
            configDir = confDir;

            string comport;
            int baud, format, stopbits, protocol;
            /** 
            * load settings  
            */
            try
            {
                // Create Settings Table
                DataTable mySettings = new DataTable("SettingsPluginTOE895x");
                mySettings.ReadXml(confDir + Path.DirectorySeparatorChar + "config_plugin_toe895x.xml");

                comport = mySettings.Rows[0]["ComPort"].ToString();
                baud = Convert.ToInt32(mySettings.Rows[0]["Baudrate"].ToString(), 10);
                format = Convert.ToInt32(mySettings.Rows[0]["FormatIndex"].ToString(), 10);
                stopbits = Convert.ToInt32(mySettings.Rows[0]["StopBitsIndex"].ToString(), 10);
                protocol = Convert.ToInt32(mySettings.Rows[0]["ProtocolIndex"].ToString(), 10);
            }
            catch
            {
                comport = "COM1";
                baud = 4800;
                format = 0;
                stopbits = 0;
                protocol = 0;
            }

            Label lblComport = new Label();
            lblComport.Text = "Comport:";
            lblComport.Top = 8;
            lblComport.Left = 5;
            lblComport.Width = 70;
            pPanel.Controls.Add(lblComport);

            Label lblBaudrate = new Label();
            lblBaudrate.Text = "Baudrate:";
            lblBaudrate.Top = 34;
            lblBaudrate.Left = 5;
            lblBaudrate.Width = 70;
            pPanel.Controls.Add(lblBaudrate);

            Label lblFormat = new Label();
            lblFormat.Text = "Format:";
            lblFormat.Top = 60;
            lblFormat.Left = 5;
            lblFormat.Width = 70;
            pPanel.Controls.Add(lblFormat);

            Label lblStopBits = new Label();
            lblStopBits.Text = "Stopbits:";
            lblStopBits.Top = 86;
            lblStopBits.Left = 5;
            lblStopBits.Width = 70;
            pPanel.Controls.Add(lblStopBits);

            Label lblProtocol = new Label();
            lblProtocol.Text = "Protokoll:";
            lblProtocol.Top = 112;
            lblProtocol.Left = 5;
            lblProtocol.Width = 70;
            pPanel.Controls.Add(lblProtocol);

            cbComport = new ComboBox();
            foreach (string port in SerialPort.GetPortNames())
            {
                cbComport.Items.Add(port);
            }
            cbComport.SelectedIndex = cbComport.Items.IndexOf(comport);
            cbComport.Top = 5;
            cbComport.Left = 84;
            cbComport.Width = 368;
            pPanel.Controls.Add(cbComport);

            cbBaudrate = new ComboBox();
            cbBaudrate.Items.Add("57600");
            cbBaudrate.Items.Add("38400");
            cbBaudrate.Items.Add("19200");
            cbBaudrate.Items.Add("9600");
            cbBaudrate.Items.Add("4800");
            cbBaudrate.Items.Add("2400");
            cbBaudrate.Items.Add("1200");
            cbBaudrate.SelectedIndex = cbBaudrate.Items.IndexOf(baud.ToString());
            cbBaudrate.Top = 31;
            cbBaudrate.Left = 84;
            cbBaudrate.Width = 368;
            pPanel.Controls.Add(cbBaudrate);

            cbFormat = new ComboBox();
            cbFormat.Items.Add("8 Bit");
            cbFormat.Items.Add("even 7 Bit");
            cbFormat.Items.Add("odd 7 Bit");
            cbFormat.SelectedIndex = format;
            cbFormat.Top = 57;
            cbFormat.Left = 84;
            cbFormat.Width = 368;
            pPanel.Controls.Add(cbFormat);

            cbStopBits = new ComboBox();
            cbStopBits.Items.Add("1");
            cbStopBits.Items.Add("2");
            cbStopBits.SelectedIndex = stopbits;
            cbStopBits.Top = 83;
            cbStopBits.Left = 84;
            cbStopBits.Width = 368;
            pPanel.Controls.Add(cbStopBits);

            cbProtocol = new ComboBox();
            cbProtocol.Items.Add("Xon/Xoff");
            cbProtocol.Items.Add("RTS/CTS");
            cbProtocol.SelectedIndex = protocol;
            cbProtocol.Top = 109;
            cbProtocol.Left = 84;
            cbProtocol.Width = 368;
            pPanel.Controls.Add(cbProtocol);
#endif
            return 0;
        }


        public int AdvancedSettingsSetup(object panel)
        {
            throw new NotImplementedException();
        }

        public int GetOutputCount()
        {
            return devInfo.outputs;
        }


        public DeviceInformation GetDeviceInformation()
        {
            return devInfo;
        }


        public int GetMinimalUpdateTimeMS()
        {
            return 10; /* 10 ms */
        }
    }
}
