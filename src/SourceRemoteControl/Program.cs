﻿/*  SourceRemoteControl
    Copyright (C) 2013  Sebastian Block

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Mono.Options;
namespace SourceRemoteControl
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

			String configString = "";
			Boolean autostart = false;
			OptionSet p = new OptionSet ()
				.Add ("autostart|a", v=> autostart=true)
				.Add ("configpath=|c=", s => configString = s);
			p.Parse (args);

			if(autostart) Console.WriteLine("auto start active");
			Form1 form1 = new Form1(configString, autostart);
            Application.Run(form1);

			return form1.ReturnCode;
        }
    }
}
